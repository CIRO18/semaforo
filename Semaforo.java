package semaforo;

import java.util.Calendar;

public class Semaforo {

    String colore = "red";

    public void on(){

        int current;

        while (true){

            if (colore == "red") {
                Calendar time = Calendar.getInstance();
                current = time.get(Calendar.SECOND);
                cambiacolore("green", current);
            }
            if (colore == "yellow") {
                Calendar time = Calendar.getInstance();
                current = time.get(Calendar.SECOND);
                cambiacolore("red", current);
            }
            if (colore == "green") {
                Calendar time = Calendar.getInstance();
                current = time.get(Calendar.SECOND);
                cambiacolore("yellow", current);
            }

        }

    }

    public void cambiacolore(String newcolor, int current){

        boolean done = false;

        while(!done) {

            Calendar time = Calendar.getInstance();

            if (current == time.get(Calendar.SECOND) - 2) {

                System.out.println(newcolor);

                colore = newcolor;

                done = true;

            }

        }

    }

}
